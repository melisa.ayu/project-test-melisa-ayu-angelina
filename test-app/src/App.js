import React, { useState } from 'react';
import Navbar from './components/navbar'
import ImageSlider from './components/slider'
import Footer from './components/footer'
import './styles/home.css'
import lightbulbIcon from './assets/lightbulb-o.png'
import bankIcon from './assets/bank.png'
import balanceScaleIcon from './assets/balance-scale.png'

function App() {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');

  const [nameError, setNameError] = useState('');
  const [emailError, setEmailError] = useState('');
  const [messageError, setMessageError] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();

    setNameError('');
    setEmailError('');
    setMessageError('');

    let isValid = true;

    if (name.trim() === '') {
      setNameError('This field is required.');
      isValid = false;
    }

    if (email.trim() === '') {
      setEmailError('This field is required.');
      isValid = false;
    } else if (!isValidEmail(email)) {
      setEmailError('Invalid email address.');
      isValid = false;
    }

    if (message.trim() === '') {
      setMessageError('This field is required.');
      isValid = false;
    }
  };

  const isValidEmail = (email) => {
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailPattern.test(email);
  };

  return (
    <div>
      <Navbar />

      <ImageSlider />

      <section className='values-section'>
        <h2>OUR VALUES</h2>
        <div className='all-values-container'>
          <div className='value-container-with-arrow'>
            <div className='value-container innovative'>
              <img src={lightbulbIcon} alt='Value icon 1' />
              <h4>INNOVATIVE</h4>
              <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime exercitationem dolorem deserunt, unde, eaque ipsa?</span>
            </div>
            <div class="triangle innovative-triangle" />
          </div>

          <div className='value-container-with-arrow'>
            <div className='value-container loyalty'>
              <img src={bankIcon} alt='Value icon 2' />
              <h4>LOYALTY</h4>
              <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit similique eum itaque facere temporibus dolores.</span>
            </div>
            <div class="triangle loyalty-triangle" />
          </div>

          <div className='value-container respect'>
            <img src={balanceScaleIcon} alt='Value icon 3' />
            <h4>RESPECT</h4>
            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, sit? Tenetur et neque quod incidunt!</span>
          </div>
        </div>
      </section>

      <section className='contact-section'>
        <h2>CONTACT US</h2>
        <div className='form-container'>
        <form className='form' onSubmit={handleSubmit}>
          <div className='form-field'>
            <label htmlFor='name'>Name</label><br />
            <input
              type='text'
              id='name'
              value={name}
              onChange={(event) => setName(event.target.value)}
              className={nameError ? 'error-border' : ''}
            />
            <div className='error-message'>{nameError}</div>
          </div>

          <div className='form-field'>
            <label htmlFor='email'>Email</label><br />
            <input
              type='text'
              id='email'
              value={email}
              onChange={(event) => setEmail(event.target.value)}
              className={emailError ? 'error-border' : ''}
            />
            <div className='error-message'>{emailError}</div>
          </div>

          <div className='form-field'>
            <label htmlFor='message'>Message</label><br />
            <textarea
              id='message'
              rows='4'
              value={message}
              onChange={(event) => setMessage(event.target.value)}
              className={messageError ? 'error-border' : ''}
            ></textarea>
            <div className='error-message'>{messageError}</div>
          </div>

          <input type='submit' value='SUBMIT' className='submit-btn' />
        </form>
        </div>
      </section>

      <Footer />
    </div>
  );
}

export default App;