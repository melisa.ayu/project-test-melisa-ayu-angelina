import React, { useEffect, useState, useRef } from 'react';
import './styles.css';
import backgroundImg from '../../assets/bg.jpg';
import aboutBackgroundImg from '../../assets/about-bg.jpg';

const ImageSlider = () => {
  const [slideIndex, setSlideIndex] = useState(1);
  const slideshowRef = useRef(null);

  const plusSlides = (n) => {
    const totalSlides = slideshowRef.current.getElementsByClassName('slides').length;
    let newIndex = slideIndex + n;
    if (newIndex > totalSlides) {
      newIndex = 1;
    } else if (newIndex < 1) {
      newIndex = totalSlides;
    }
    setSlideIndex(newIndex);
  };

  useEffect(() => {
    const showSlides = (n) => {
      const slides = slideshowRef.current.getElementsByClassName('slides');
      const dots = slideshowRef.current.getElementsByClassName('dot');

      if (n > slides.length) {
        setSlideIndex(1);
      }
      if (n < 1) {
        setSlideIndex(slides.length);
      }

      for (let i = 0; i < slides.length; i++) {
        slides[i].style.display = 'none';
      }
      for (let i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(' active', '');
      }

      slides[slideIndex - 1].style.display = 'block';
      dots[slideIndex - 1].className += ' active';
    };

    showSlides(slideIndex);
  }, [slideIndex]);

  return (
    <div className='slides-container' ref={slideshowRef}>
      <div className='slides'>
        <img src={backgroundImg} alt='Slide 1' />
        <div className='caption-container'>
          <span className='caption'>
            THIS IS A PLACE WHERE TECHNOLOGY &<br />
            CREATIVITY FUSED INTO DIGITAL CHEMISTRY
          </span>
        </div>
      </div>

      <div className='slides'>
        <img src={aboutBackgroundImg} alt='Slide 2' />
        <div className='caption-container'>
          <span className='caption'>
            WE DON'T HAVE THE BEST BUT WE HAVE THE<br />
            GREATEST TEAM
          </span>
        </div>
      </div>

      <a className='prev' onClick={() => plusSlides(-1)}>❮</a>
      <a className='next' onClick={() => plusSlides(1)}>❯</a>

      <div className='dot-container'>
        <span className='dot'/>
        <span className='dot'/>
      </div>
    </div>
  );
};

export default ImageSlider;