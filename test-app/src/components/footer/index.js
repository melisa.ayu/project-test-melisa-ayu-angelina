import './styles.css'
import facebookIcon from '../../assets/facebook-official.png';
import twitterIcon from '../../assets/twitter.png';

const Footer = () => {
  return (
    <footer>
      <div className='footer-item-container'>
        <span>Copyright © 2016. PT Company</span>

        <nav className='socials-container'>
          <a href="#">
            <img src={facebookIcon} alt='Facebook' />
          </a>
          <a href="#">
            <img src={twitterIcon} alt='Twitter' />
          </a>
        </nav>
      </div>
    </footer>
  );
}

export default Footer;