import './styles.css'

const Navbar = () => {
  return (
    <header>
      <div className='navbar'>
        <div className='nav-item-container'>
          <div className='logo'>
            Company
          </div>

          <nav className='menu'>
            <div className='about-menu'>
              <span>ABOUT</span>
              <div className='about-dropdown'>
                <a href="#">HISTORY</a>
                <a href="#">VISION MISSION</a>
              </div>
            </div>
            <a href="#">OUR WORK</a>
            <a href="#">OUR TEAM</a>
            <a href="#">CONTACT</a>
          </nav>
        </div>
      </div>
    </header>
  );
}

export default Navbar;